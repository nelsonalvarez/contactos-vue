import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({

    theme: {
        themes: {
          light: {
            primary: '#505257',
            secondary: '#FAB428',
            accent: '#638273',
            error: '#F65858',
            llamadaApi: '#A7FF90'
          },
        },
      }

});
