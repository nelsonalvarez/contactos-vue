import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import VueToastify from 'vue-toastify';
import '@fortawesome/fontawesome-free/css/all.css'
import "./assets/hover.css";

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import axios from 'axios';
import VueAxios from 'vue-axios';

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';


Vue.config.productionTip = false

//Vue.use(ElementUI)
Vue.use(vuetify);
Vue.use(ElementUI);
Vue.use(VueToastify);
Vue.use(VueSweetalert2);

Vue.use(axios, VueAxios);

// Ruta default api
axios.defaults.baseURL = 'http://localhost:3000/viajes/v1'

new Vue({
  router,
  store,
  vuetify,
  icons: {
    iconfont: 'fa',
  },
  render: h => h(App)
}).$mount('#app')
