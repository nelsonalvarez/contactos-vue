import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  //{
  //  path: '/presupuestos',
  //  name: 'presupuestos',
  //  component: () => import(/* webpackChunkName: "presupuestos" */ '../views/Presupuestos.vue')
  //},

  {
    path: '/subastador',
    name: 'subastador',
    component: () => import(/* webpackChunkName: "presupuestos" */ '../views/Subastador.vue')
  }


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
