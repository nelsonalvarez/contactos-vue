import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    // STATE
    movil: {
      origen: '',
      disco: '',
      ubicacion_disco: '',
      fecha: new Date().toLocaleDateString(),
      distancia: ''
    },

    
    // STATE QUE CONTIENE EL ARRAY DE PRESUPUESTOS
    movilesSubasta: []

  },
  mutations: {

    

  },
  actions: {

    

   

  }
})
